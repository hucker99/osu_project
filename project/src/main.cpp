#include <iostream>
#include "AudioFile.h"
#include <vector>
#define _USE_MATH_DEFINES
#include <cmath>


int main() {
    AudioFile<double> audioFile;
    audioFile.load("./Cybor.wav");
    // 1. Create an AudioBuffer
// (BTW, AudioBuffer is just a vector of vectors)
    int numSamples = audioFile.getNumSamplesPerChannel();
    int numChannels = audioFile.getNumChannels();
    AudioFile<double>::AudioBuffer bigbuffer;
    AudioFile<double>::AudioBuffer smallbuffer;
    AudioFile<double>::AudioBuffer savebuffer;
// 2. Set to (e.g.) two channels
    bigbuffer.resize (numChannels);
    smallbuffer.resize (numChannels);
    savebuffer.resize (numChannels);
    int bigbuf_samples = 44032;
    int smallbuf_samples = 1024;
// 3. Set number of samples per channel
    for (int channel = 0; channel < numChannels; ++channel) {
        for (int i = 0; i < bigbuf_samples-smallbuf_samples; ++i) {
            bigbuffer[channel].push_back(audioFile.samples[channel][i]);
        }
    }

    for (int channel = 0; channel < numChannels; ++channel) {
        for (int i = bigbuf_samples-smallbuf_samples; i < bigbuf_samples; ++i) {
            bigbuffer[channel].push_back(audioFile.samples[channel][i]);
            smallbuffer[channel].push_back(audioFile.samples[channel][i]);
        }
    }

    int k = 43;
    double beat_constant = 1.3;
    int count_of_mini_samples = numSamples/smallbuf_samples;
    while (k < count_of_mini_samples) {
        double small_sum = 0;
        double big_sum = 0;
        for (int channel = 0; channel < numChannels; ++channel) {
            for (int i = 0; i < bigbuf_samples-smallbuf_samples; ++i) {
                big_sum += bigbuffer[channel][i] * bigbuffer[channel][i];
            }
        }

        for (int channel = 0; channel < numChannels; ++channel) {
            for (int i = smallbuf_samples; i > 0; --i) {
                big_sum += bigbuffer[channel][bigbuf_samples - i] * bigbuffer[channel][bigbuf_samples - i];
                small_sum += bigbuffer[channel][i-1] * bigbuffer[channel][i-1];
            }
        }

        double avg_E = (double)smallbuf_samples/bigbuf_samples * big_sum;
        if (small_sum > beat_constant * avg_E) {
            for (int channel = 0; channel < numChannels; ++channel) {
                for (int i = 0; i < smallbuf_samples; ++i) {
                    savebuffer[channel].push_back(smallbuffer[channel][i]);
                }
            }
        }

        for (int channel = 0; channel < numChannels; ++channel) {
            bigbuffer[channel].erase(bigbuffer[channel].begin(), bigbuffer[channel].begin() + smallbuf_samples);
            smallbuffer[channel].clear();
        }

        for (int channel = 0; channel < numChannels; ++channel) {
            for (int i = k * smallbuf_samples; i < (k + 1) * smallbuf_samples; ++i) {
                bigbuffer[channel].push_back(audioFile.samples[channel][i]);
                smallbuffer[channel].push_back(audioFile.samples[channel][i]);
            }
        }
        ++k;
    }
    audioFile.setAudioBuffer (savebuffer);
    audioFile.save ("./audioFile.wav");




// 4. do something here to fill the buffer with samples, e.g.


    return 0;

}

